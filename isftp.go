package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
)

type IsftpImpl struct {
	Iftp
	conn   *sftp.Client
	client *ssh.Client
}

// PublicKeyFile reads an ssh keyfile and return an shh.AuthMethod
// this wants the private key file
func PublicKeyFile(file string) ssh.AuthMethod {
	buffer, err := ioutil.ReadFile(file)
	if err != nil {
		log.Errorf("can't read %s, %v", file, err)
		return nil
	}

	key, err := ssh.ParsePrivateKey(buffer)
	if err != nil {
		log.Errorf("error parsing %s, %v", file, err)
		return nil
	}
	return ssh.PublicKeys(key)
}

// NewSftpImpl return a new connection thingy
func NewSftpImpl() *IsftpImpl {
	is := IsftpImpl{
		conn:   new(sftp.Client),
		client: new(ssh.Client),
	}
	return &is
}

// Connect to the host
func (f *IsftpImpl) Connect(c *Options) error {
	var err error
	var port = c.Port

	if port == 0 {
		port = 22
	}

	var auths []ssh.AuthMethod

	if c.Host == "" {
		return fmt.Errorf("Host not specified")
	}
	if c.Pass != "" {
		auths = append(auths, ssh.Password(c.Pass))
	}
	if c.Pkey != "" {
		keyauth := PublicKeyFile(c.Pkey)
		if keyauth != nil {
			auths = append(auths, keyauth)
		}
	}

	config := ssh.ClientConfig{
		User:            c.User,
		Auth:            auths,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	addr := fmt.Sprintf("%s:%d", c.Host, port)
	if f.client, err = ssh.Dial("tcp", addr, &config); err != nil {
		return fmt.Errorf("unable to connect: %s", err)
	}

	if f.conn, err = sftp.NewClient(f.client, sftp.MaxPacket(1<<15)); err != nil {
		return fmt.Errorf("unable to start sftp: %s", err)
	}

	return nil
}

// Close the connection
func (f *IsftpImpl) Close() error {
	if f.conn == nil {
		return nil
	}
	if err := f.conn.Close(); err != nil {
		return fmt.Errorf("unable to close sftp: %v", err)
	}
	if err := f.client.Close(); err != nil {
		return fmt.Errorf("unable to close sftp client: %v", err)
	}

	return nil
}

// Curdir returns the current directory
func (f *IsftpImpl) Curdir() (string, error) {
	return f.conn.Getwd()
}

// Chdir is a noop in sftp
func (f *IsftpImpl) Chdir(string) error {
	return nil //fmt.Errorf("ChDir is a noop")
}

// Mkdir creates a directory if it doesn't exist already
func (f *IsftpImpl) Mkdir(path string) error {
	var err error
	// check dir doesn't alreay exist
	if _, err = f.conn.Stat(path); err != nil {
		if err = f.conn.Mkdir(path); err != nil {
			return fmt.Errorf("error: %v", err)
		}
	}
	return nil
}

// Mkdirs creates a path if it doesn't exists already
func (f *IsftpImpl) Mkdirs(path string) error {
	var err error
	var fpath string

	fpath = "/"
	chunks := strings.Split(path, "/")

	for i := range chunks {
		if chunks[i] == "" {
			continue
		}
		fpath = sftp.Join(fpath, chunks[i])
		if err = f.Mkdir(fpath); err != nil {
			return fmt.Errorf("error: %s", err)
		}
		//fmt.Println(fpath)
	}

	return nil
}

// Dir return a list of files in path
func (f *IsftpImpl) Dir(path string) ([]string, error) {

	var err error
	var slist []string
	var filist []os.FileInfo

	if filist, err = f.conn.ReadDir(path); err != nil {
		return nil, fmt.Errorf("error: %v", err)
	}

	for i := 0; i < len(filist); i++ {
		slist = append(slist, filist[i].Name())
	}
	return slist, nil
}

// Download a file
func (f *IsftpImpl) Download(rpath string, lpath string) error {

	src, err := f.conn.Open(rpath)
	if err != nil {
		log.Errorf("%s not found on remote", rpath)
		os.Exit(1)
	}
	defer src.Close()
	dst, err := os.Create(lpath)
	if err == nil {
		src.WriteTo(dst)
		dst.Close()
	}

	return nil
}

// Upload a file
func (f *IsftpImpl) Upload(lpath string, rpath string) error {

	bytes, err := ioutil.ReadFile(lpath)
	if err != nil {
		return fmt.Errorf("error: %s", err)
	}

	rfile, err := f.conn.Create(rpath)
	if err != nil {
		return fmt.Errorf("create Error: %s, %v", rpath, err)
	}
	if _, err = rfile.Write(bytes); err != nil {
		rfile.Close()
		return fmt.Errorf("write Error: %s, %v", rpath, err)
	}
	rfile.Close()

	return nil
}

// UploadBytes to a file
func (f *IsftpImpl) UploadBytes(rpath string, b []byte) error {

	rfile, err := f.conn.Create(rpath)
	if err != nil {
		return fmt.Errorf("create Error: %s, %v", rpath, err)
	}
	if _, err = rfile.Write(b); err != nil {
		rfile.Close()
		return fmt.Errorf("write Error: %s, %v", rpath, err)
	}
	rfile.Close()

	return nil
}

// Delete a file
func (f *IsftpImpl) Delete(path string) error {

	if err := f.conn.Remove(path); err != nil {
		//return fmt.Errorf("Error: %s", err)
		log.Errorf("failed to remove %s, %v", path, err)
	}
	return nil
}

// Exists return true if a file exists
func (f *IsftpImpl) Exists(path string) (bool, error) {

	if _, err := f.conn.Stat(path); err != nil {
		return false, fmt.Errorf("path does not exist: %s, %v", path, err)
	}

	return true, nil
}
