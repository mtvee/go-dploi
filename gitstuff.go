package main

import (
	"fmt"
	"os/exec"
	"strings"
)

// run a shell command and return a string array of output
func runCmd(cmd string, args []string) ([]string, error) {

	var lines []string

	cmdOut, err := exec.Command(cmd, args...).Output()
	if err != nil {
		return lines, fmt.Errorf("There was an error running %s, %v", cmd, err)
	}

	str := string(cmdOut)
	lines = strings.Split(str, "\n")
	return lines, nil
}

// get the current git SHA
func gitCurrentSha() (string, error) {
	lines, err := runCmd("git", []string{"rev-parse", "--verify", "--max-parents=0", "HEAD"})
	if err != nil {
		return "", fmt.Errorf("%v", err)
	}

	currentHash := lines[0]
	return currentHash, nil
}

// get list of changed files
// NOTE this can die if zero or only one commit
// git diff --name-only --no-renames --diff-filter=AM
func gitChangedFiles(fromSha string) ([]string, error) {
	lines, err := runCmd("git", []string{"diff", "--name-only", "--no-renames", "--diff-filter=AM", fromSha, "HEAD"})
	if err != nil {
		return nil, fmt.Errorf("%v", err)
	}
	return lines, nil
}

// get list of deleted files
// NOTE this can die if zero or only one commit
// git diff --name-only --no-renames --diff-filter=D
func gitDeletedFiles(fromSha string) ([]string, error) {
	lines, err := runCmd("git", []string{"diff", "--name-only", "--no-renames", "--diff-filter=D", fromSha, "HEAD"})
	if err != nil {
		return nil, fmt.Errorf("%v", err)
	}
	return lines, nil
}
