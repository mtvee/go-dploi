From: https://github.com/onjin/docker-alpine-vsftpd


Check that `docker-entrypoint.sh` script has unix line endings

Build with:

    docker build -t mtvee/vsftpd .

User name is 'files'

Run with: (will generate a random password)
    
    docker run -it --rm -v ${PWD}/files:/home/files mtvee/vsftpd 

or set the password with environment PASSWORD to 'files'

    docker run -it --rm  -v ${PWD}/files:/home/files -e PASSWORD=files mtvee/vsftpd
