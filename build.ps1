# go build|install with git has and time
# somewhere in the package main have...
# var sha1ver string
# var buildTime string
# --------------------------------------------------------------------

param (
    [switch]$install = $false
)

$now = Get-Date -UFormat "%Y-%m-%d_%T"
$sha1 = (git rev-parse HEAD).Trim().SubString(0,8)
$cmd = "build"

if($install) 
{
    $cmd = "install"
}

go "$cmd" -ldflags "-X main.sha1ver=$sha1 -X main.buildTime=$now" 
Write-Host "$cmd done!"