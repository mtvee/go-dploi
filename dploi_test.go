package main

import "testing"

func TestEnsurePath(t *testing.T) {
	actualResult := "olleH"
	var expectedResult = "olleH"

	if actualResult != expectedResult {
		t.Fatalf("Expected %s but got %s", expectedResult, actualResult)
	}
}
