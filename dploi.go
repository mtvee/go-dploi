package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"strings"

	"path/filepath"

	"github.com/pkg/sftp"

	"github.com/op/go-logging"
)

// version
const versionString string = "0.4.4"

// these can be set when compiling e.g.
// go build -ldflags "-X main.sha1ver=$sha1 -X main.buildTime=$now"
var sha1ver string
var buildTime string

// filename of the git hash file that store the last SHA on the server
const gitHashFileName string = "git.hash"

// a logger
var log = logging.MustGetLogger("dploi")

// Options cli options
type Options struct {
	Debug    bool
	User     string `json:"user"`
	Pass     string `json:"pass"`
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Pkey     string `json:"pkey"`
	Rroot    string `json:"remote_root"`
	Sha      string `json:"sha"`
	HashFile string `json:"hash_filename"`
	Dry      bool
	Config   string
}

// get first commit
// git rev-list --max-parents=0 HEAD

// substiute ~ with env.HOME
func replaceEnv(s string) string {
	home := os.Getenv("HOME")
	if home == "" {
		// windows i think
		home = os.Getenv("HOMEPATH")
		if home == "" {
			home = "~"
		}
	}
	if strings.Index(s, "~") != -1 {
		s = strings.Replace(s, "~", home, -1)
	}
	return s
}

// initialize the sftp connection
func sftpInit(conn *IsftpImpl, c *Options) error {
	var err error

	if err = conn.Connect(c); err != nil {
		return err
	}

	defer func() {
		// get rid of the local hash file if it exists
		if _, err := os.Stat(c.HashFile); err == nil {
			err = os.Remove(c.HashFile)
			if err != nil {
				log.Errorf("unable to delete local %s, %v", c.HashFile, err)
			}
		}
	}()

	var curpath string
	if curpath, err = conn.Curdir(); err != nil {
		return fmt.Errorf("Can't get remote pwd: %s", err)
	}

	// check if we are absolute or local path
	if !strings.HasPrefix(c.Rroot, "/") {
		c.Rroot = sftp.Join(curpath, c.Rroot)
	}

	var exists bool
	if exists, err = conn.Exists(c.Rroot); err != nil {
		return err
	}
	if !exists {
		return fmt.Errorf("Remote root does not exist")
	}

	log.Infof("Remote path: %s", c.Rroot)

	// no sha supplied so see if there is one on the server
	if c.Sha == "" {
		if err = conn.Download(sftp.Join(c.Rroot, c.HashFile), c.HashFile); err != nil {
			return err
		}
		var b []byte
		b, err = ioutil.ReadFile(c.HashFile) // just pass the file name
		if err != nil {
			return err
		}
		c.Sha = strings.TrimSpace(string(b))
	}

	return nil
}

// push via ssh
func cmdPushSftp(c *Options) error {
	var err error
	var conn = NewSftpImpl()

	if err = sftpInit(conn, c); err != nil {
		return err
	}
	defer func() {
		log.Info("closing connection")
		conn.Close()
	}()

	var currentHash string
	if currentHash, err = gitCurrentSha(); err != nil {
		return err
	}

	log.Info("   From SHA:", c.Sha)
	log.Info("Current SHA:", currentHash)

	if c.Sha == currentHash {
		log.Info("SHAs match, nothing to do!")
		return nil
	}

	var files []string
	if files, err = gitChangedFiles(c.Sha); err != nil {
		return err
	}

	log.Info("pushing changes...")
	//fmt.Println(lines)

	for _, p := range files {
		if p == "" {
			continue
		}
		dir, _ := filepath.Split(p)
		rpath := sftp.Join(c.Rroot, p)

		if !c.Dry {
			// make sure the path exists
			if err = conn.Mkdirs(sftp.Join(c.Rroot, dir)); err != nil {
				return err
			}
			if err = conn.Upload(p, rpath); err != nil {
				return err
			}
		}
		log.Infof("sent: %s", rpath)

	}

	log.Info("done sending")

	log.Info("checking for deletions...")

	if files, err = gitDeletedFiles(c.Sha); err != nil {
		return err
	}

	for _, p := range files {
		if p == "" {
			continue
		}
		rpath := sftp.Join(c.Rroot, p)
		if !c.Dry {
			//if err = sftpc.Remove(rpath); err != nil {
			if err = conn.Delete(rpath); err != nil {
				//return fmt.Errorf("Error: %s", err)
				log.Errorf("failed to remove %s, %v", rpath, err)
			} else {
				log.Infof("removed: %s", rpath)
			}
		} else {
			log.Infof("removed: %s", rpath)
		}
	}

	log.Info("sending hash...")

	// write the current hash back to the server
	rpath := sftp.Join(c.Rroot, c.HashFile)
	if !c.Dry {
		if err = conn.UploadBytes(rpath, []byte(currentHash)); err != nil {
			return err
		}
	}
	log.Infof("all done: %s", currentHash)

	return nil
}

// initialize the ftp connection
func ftpInit(conn *IftpImpl, c *Options) error {
	var err error

	if err = conn.Connect(c); err != nil {
		return err
	}

	defer func() {
		// get rid of the local hash file if it exists
		if _, err := os.Stat(c.HashFile); err == nil {
			err = os.Remove(c.HashFile)
			if err != nil {
				log.Errorf("unable to delete %s, %v", c.HashFile, err)
			}
		}
	}()

	log.Infof("Successfully connected to %s", c.Host)

	var curpath string
	if curpath, err = conn.Curdir(); err != nil {
		return fmt.Errorf("Can't get remote pwd: %s", err)
	}

	// check if we are absolute or local path
	if !strings.HasPrefix(c.Rroot, "/") {
		c.Rroot = sftp.Join(curpath, c.Rroot)
	}
	log.Infof("Remote path: %s", c.Rroot)

	// change to remote root
	if err = conn.Chdir(c.Rroot); err != nil {
		return fmt.Errorf("Can't change to remote root: %s", err)
	}

	// no sha supplied so see if there is one on the server
	if c.Sha == "" {
		if err = conn.Download(c.HashFile, c.HashFile); err != nil {
			log.Errorf("%s not found on remote. aborting!", c.HashFile)
			os.Exit(1)
		}

		var b []byte
		b, err = ioutil.ReadFile(c.HashFile) // just pass the file name
		if err != nil {
			return err
		}
		c.Sha = string(b)
	}

	return nil
}

// push ftp command
func cmdPushFtp(c *Options) error {
	var err error
	var conn = NewFtpImpl()

	if err = ftpInit(conn, c); err != nil {
		return err
	}
	defer func() {
		log.Info("closing connection")
		conn.Close()
	}()

	var currentHash string
	if currentHash, err = gitCurrentSha(); err != nil {
		return err
	}

	log.Info("   From SHA: ", c.Sha)
	log.Info("Current SHA: ", currentHash)

	if c.Sha == currentHash {
		log.Info("SHAs match, nothing to do!")
		return nil
	}

	var files []string
	if files, err = gitChangedFiles(c.Sha); err != nil {
		return err
	}

	log.Info("pushing changes...")

	for _, p := range files {
		if p == "" {
			continue
		}
		dir, _ := filepath.Split(p)
		// make sure the path exists
		if !c.Dry {
			if err = conn.Mkdirs(dir); err != nil {
				return err
			}
			if err = conn.Upload(p, p); err != nil {
				return err
			}
		}
		log.Infof("sent: %s", p)
	}

	// check for deleted files
	if files, err = gitDeletedFiles(c.Sha); err != nil {
		return err
	}

	for _, p := range files {
		if p == "" {
			continue
		}
		if !c.Dry {
			if err = conn.Delete(p); err != nil {
				//return fmt.Errorf("Error: %s", err)
				log.Errorf("failed to remove %s, %s", p, err)
			} else {
				log.Infof("removed: %s", p)
			}
		} else {
			log.Infof("removed: %s", p)
		}
	}

	if !c.Dry {
		// write the current hash back to the server
		if err = conn.UploadBytes(c.HashFile, []byte(currentHash)); err != nil {
			log.Errorf("unable to write to remote %s", c.HashFile)
		}
	}

	log.Infof("sent: %s", c.HashFile)

	return nil
}

// figure which protocol and call
func cmdPush(opts *Options) error {
	u, err := url.Parse(opts.Host)
	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(3)
	}
	opts.Host = u.Host
	switch u.Scheme {
	case "ftp":
		err = cmdPushFtp(opts)
		if err != nil {
			return err
		}
	case "sftp":
		err = cmdPushSftp(opts)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("Error: unknown scheme %s", u.Scheme)
	}

	return nil
}

// check ftp status
func cmdStatusFtp(opts *Options) error {
	var err error
	var conn = NewFtpImpl()

	if err = ftpInit(conn, opts); err != nil {
		return err
	}
	defer func() {
		log.Info("closing connection")
		conn.Close()
	}()

	var currentHash string
	if currentHash, err = gitCurrentSha(); err != nil {
		return err
	}

	log.Info(" Remote SHA: ", opts.Sha)
	log.Info("  Local SHA: ", currentHash)

	return nil
}

// check sftp status
func cmdStatusSftp(opts *Options) error {
	var err error
	var conn = NewSftpImpl()

	if err = sftpInit(conn, opts); err != nil {
		return err
	}
	defer func() {
		log.Info("closing connection")
		conn.Close()
	}()

	var currentHash string
	if currentHash, err = gitCurrentSha(); err != nil {
		return err
	}

	log.Info("   From SHA:", opts.Sha)
	log.Info("Current SHA:", currentHash)

	if opts.Sha == currentHash {
		log.Info("SHAs match, nothing to do!")
		return nil
	}

	return nil
}

// figure which handler to call based on protocol
func cmdStatus(opts *Options) error {
	// if user supplied SHA we clear it to force grab from server
	opts.Sha = ""
	u, err := url.Parse(opts.Host)
	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(3)
	}
	opts.Host = u.Host
	switch u.Scheme {
	case "ftp":
		err = cmdStatusFtp(opts)
		if err != nil {
			return err
		}
	case "sftp":
		err = cmdStatusSftp(opts)
		if err != nil {
			return err
		}
	default:
		return fmt.Errorf("Error: unknown scheme %s", u.Scheme)
	}

	return nil
}

// print out a usage screen
func usage() {
	fmt.Printf("dploi v%s %s %s\n", versionString, sha1ver, buildTime)
	fmt.Println("Usage: dploi <command> [<args>]")
	fmt.Println("")
	fmt.Println("COMMANDS")
	fmt.Println("\thelp     this")
	fmt.Println("\tpush     push changes to the server")
	fmt.Println("\tstatus   compare local to remote SHA")
	fmt.Println("\tversion  print version info")
	fmt.Println("")
	fmt.Println("ARGS")
	fmt.Println("\t-debug             output debug info")
	fmt.Println("\t-config FILE       dploi.json file to use")
	fmt.Println("\t-dry-run           run the push but don't transfer")
	fmt.Println("\t-host HOST         host name")
	fmt.Println("\t-port PORT         port")
	fmt.Println("\t-pass PASS         password")
	fmt.Println("\t-pkey FILE         ssh private key file")
	fmt.Println("\t-remote-root PATH  remote root directory")
	fmt.Println("\t-sha SHA           the local SHA to push since")
	fmt.Println("\t-user USER         username")
}

// ------------------------------------------------------------------
// M A I N
// ------------------------------------------------------------------
func main() {

	// need a command
	if len(os.Args) == 1 {
		usage()
		return
	}

	if os.Args[1] == "version" {
		fmt.Printf("dploi v%s %s %s\n", versionString, sha1ver, buildTime)
		os.Exit(0)
	}

	if os.Args[1] == "help" {
		usage()
		os.Exit(0)
	}

	backend1 := logging.NewLogBackend(os.Stderr, "", 0)
	backend1Leveled := logging.AddModuleLevel(backend1)
	backend1Leveled.SetLevel(logging.INFO, "")

	// Set the backends to be used.
	logging.SetBackend(backend1Leveled)

	var opts Options
	var copts Options

	pushCommand := flag.NewFlagSet("push", flag.ExitOnError)

	pushCommand.BoolVar(&copts.Debug, "debug", false, "Output debug info")
	pushCommand.StringVar(&copts.Host, "host", "", "Host to connect to")
	pushCommand.IntVar(&copts.Port, "port", 0, "Port")
	pushCommand.StringVar(&copts.User, "user", "anonymous", "User name")
	pushCommand.StringVar(&copts.Pass, "pass", "anonymous", "Password")
	pushCommand.StringVar(&copts.Pkey, "pkey", "", "SSH private key file")
	pushCommand.StringVar(&copts.Sha, "sha", "", "local SHA to push since")
	pushCommand.StringVar(&copts.Rroot, "remote-root", "", "remote root directory")
	pushCommand.StringVar(&copts.HashFile, "hash-filename", gitHashFileName, "hash file name")
	pushCommand.BoolVar(&copts.Dry, "dry-run", false, "Dry run")
	pushCommand.StringVar(&copts.Config, "config", "dploi.json", "dploi.json file to use")

	pushCommand.Parse(os.Args[2:])

	// check for config file first, the cli will override
	if copts.Config != "" {
		if _, err := os.Stat("./" + copts.Config); err != nil {
			fmt.Printf("Error: can't open: %s", copts.Config)
			os.Exit(4)
		} else {
			raw, err := ioutil.ReadFile("./" + copts.Config)
			if err != nil {
				fmt.Println(err)
			}
			json.Unmarshal(raw, &opts)
		}
	}

	if copts.User != "anonymous" {
		opts.User = copts.User
	}
	if copts.Pass != "anonymous" {
		opts.Pass = copts.Pass
	}
	if copts.Host != "" {
		opts.Host = copts.Host
	}
	if copts.Port != 0 {
		opts.Port = copts.Port
	}
	if copts.Pkey != "" {
		opts.Pkey = replaceEnv(copts.Pkey)
	}
	if copts.Rroot != "" {
		opts.Rroot = copts.Rroot
	}
	if copts.Debug != false {
		opts.Debug = true
	}
	if copts.Sha != "" {
		opts.Sha = copts.Sha
	}
	fmt.Printf("[%q]", copts.HashFile)
	if copts.HashFile != gitHashFileName {
		opts.HashFile = copts.HashFile
	}
	if opts.HashFile == "" {
		opts.HashFile = gitHashFileName
	}
	opts.Dry = copts.Dry

	opts.Pkey = replaceEnv(opts.Pkey)

	// check there is a .git repo here
	if _, err := os.Stat("./.git"); err != nil {
		fmt.Println(".git not found. please run this from the root of the repo")
		os.Exit(1)
	}

	switch os.Args[1] {
	case "push":
		if err := cmdPush(&opts); err != nil {
			fmt.Printf("%v", err)
		}
	case "status":
		if err := cmdStatus(&opts); err != nil {
			fmt.Printf("%v", err)
		}
	default:
		fmt.Printf("%q is not valid command.\n", os.Args[1])
		usage()
		os.Exit(2)
	}
}
