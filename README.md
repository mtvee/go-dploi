dploi
=====

Deploy files via S/FTP using git hash as a waterline.

`dploi` utilizes a historical git SHA to determine which files to
upload to a host. It keeps a file on the host that specifies the
last SHA pushed and does a diff on the tree to collect the changes
and push them. Alternately you can specify the historical SHA via
the command line.

Building
--------
    $> git clone git@gitlab.com:mtvee/go-dploi.git dploi
    $> cd dploi
    $> go get
    $> go build

Commands
--------

`push`    push changes via ftp

`status`  compare remote and local SHA

`version` prints out version

Flags are:

  `-debug`        turn on debugging output. shows commands going to and
                  from the server.

  `-sha`          the historical SHA to push from

  `-host`         the host name to connect to, requires a scheme, either
                  ftp://host or sftp://host

  `-user`         the username on the host

  `-pass`         the password for the host

  `-remote-root`  the remote root directory for transfers

  `-port PORT`    port to connect to if non-standard

  `-pkey`         path to private key file 

  `-config`       alternate configuration file

  `-dry-run`      go through the motions but no data is transferred


You can create a file called `dploi.json` in the root of the repo to specify most of the command line flags:

    {
        "host" : "sftp://127.0.0.1",
        "port" : 22,
        "user" : "junk",
        "pass" : "letmein",
        "remote_root" : "ftptest",
        "pkey" : "~/.ssh/id_rsa"
    }


Example Usage
-------------
- push via ftp from the last revision. needs `dploi.json` in the root of the repo

    `dploi push -sha HEAD~1`

- push via sftp from a specific revision. needs `dploi-test.json` in the root of the repo

    `dploi push -sha cdb9f313a90bb1a78aa24576d159826c48ff9110 -config dploi-test.json` 

- push from 3 revisions back and specify everything on the command line

    `dploi push -sha HEAD~3 -host ftp://ftp.somewhere.arg -user me -pass letmein`

- check the status of a server

    `dploi status -host sftp://192.168.1.10 -pkey ~/.ssh/id_rsa`

Notes
-----

I am new to `go` so this seemed like a fun place to start learning. That being said, probably a lot of what you see in the code is not idiomatic `go`. Hopefully as I learn more it will get better. 

Any input is appreciated!! 

- *like a sensible way to `go test` this thing :P*


TODO
----

- figure out how to write tests (docker?)
- possible other commands to init, push just SHA, get SHA only from server, push all revisions
- have ignore/include wildcards
- goftp package doesn't do ipv6

