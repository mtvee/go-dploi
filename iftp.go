package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"strings"

	"gopkg.in/dutchcoders/goftp.v1"

	"os"

	"github.com/pkg/sftp"
)

// Iftp abstraction of s/ftp
type Iftp interface {
	Connect(c *Options) error
	Close() error
	Curdir() (string, error)
	Chdir(string) error
	Mkdir(string) error
	Mkdirs(string) error
	Dir(string) ([]string, error)
	Download(string, string) error
	Upload(string, string) error
	UploadBytes(string, []byte) error
	Delete(string) error
	Exists(string) (bool, error)
}

type IftpImpl struct {
	Iftp
	conn *goftp.FTP
}

// see if a filename exists in a slice from ftp.List
// there is a newline at the end of these lines
// type=file;modify=20170429015442;size=2129; README.md
// type=dir;modify=20170428132329; src
func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if strings.HasSuffix(strings.TrimSpace(b), a) {
			return true
		}
	}
	return false
}

// =======================================================
// ftp stuff
// =======================================================

// NewFtpImpl return a new connection thingy
func NewFtpImpl() *IftpImpl {
	fi := IftpImpl{
		conn: new(goftp.FTP),
	}
	return &fi
}

// Connect to the host
func (f *IftpImpl) Connect(c *Options) error {
	var err error
	var port = c.Port

	if port == 0 {
		port = 21
	}

	if c.Host == "" {
		return fmt.Errorf("Host not specified")
	}

	addr := fmt.Sprintf("%s:%d", c.Host, port)
	if c.Debug {
		// For debug messages: goftp.ConnectDbg("ftp.server.com:21")
		if f.conn, err = goftp.ConnectDbg(addr); err != nil {
			return fmt.Errorf("Error: %s", err)
		}
	} else {
		if f.conn, err = goftp.Connect(addr); err != nil {
			return fmt.Errorf("Error: %s", err)
		}
	}

	// Username / password authentication
	if err = f.conn.Login(c.User, c.Pass); err != nil {
		return fmt.Errorf("Login error: %s", err)
	}

	return nil
}

// Close the connection
func (f *IftpImpl) Close() error {
	if f.conn == nil {
		return nil
	}
	if err := f.conn.Close(); err != nil {
		return fmt.Errorf("error: %v", err)
	}
	return nil
}

// Curdir returns the current directory
func (f *IftpImpl) Curdir() (string, error) {
	var curpath string
	var err error

	if curpath, err = f.conn.Pwd(); err != nil {
		return "", fmt.Errorf("Can't get remote pwd: %s", err)
	}
	return curpath, nil
}

// Chdir switchs to path
func (f *IftpImpl) Chdir(path string) error {
	if err := f.conn.Cwd(path); err != nil {
		return fmt.Errorf("Can't change to: %s, %v", path, err)
	}
	return nil
}

// Mkdir creates a directory if it doesn't exist already
func (f *IftpImpl) Mkdir(path string) error {
	var err error
	var exists bool

	if exists, err = f.Exists(path); err != nil {
		return err
	}
	if !exists {
		if err = f.conn.Mkd(path); err != nil {
			return fmt.Errorf("Can't create: %s, %v", path, err)
		} else {
			fmt.Println("created dir", path)
		}
	}
	return nil
}

// Mkdirs creates a path if it doesn't exist already
func (f *IftpImpl) Mkdirs(path string) error {

	var fpath string
	var err error

	chunks := strings.Split(path, "/")

	for i := range chunks {
		if chunks[i] == "" {
			continue
		}
		fpath = sftp.Join(fpath, chunks[i])
		if err = f.Mkdir(fpath); err != nil {
			return fmt.Errorf("error: %s", err)
		}
		//fmt.Println(fpath)
	}

	return nil
}

// Dir returns a list of files from path
func (f *IftpImpl) Dir(path string) ([]string, error) {
	var slist []string
	var err error

	if slist, err = f.conn.List(path); err != nil {
		return nil, fmt.Errorf("Error: %s", err)
	}
	return slist, nil
}

// Download a file
func (f *IftpImpl) Download(rpath string, lpath string) error {
	var err error

	_, err = f.conn.Retr(rpath, func(r io.Reader) error {
		data, err := ioutil.ReadAll(r)
		if err != nil {
			return err
		}
		err = ioutil.WriteFile(lpath, data, 0644)
		if err != nil {
			return err
		}
		return nil
	})
	return err
}

// Upload a file
func (f *IftpImpl) Upload(lpath string, rpath string) error {
	var file *os.File
	var err error

	if file, err = os.Open(lpath); err != nil {
		return fmt.Errorf("Error: %s", err)
	}

	if err = f.conn.Stor(rpath, file); err != nil {
		return fmt.Errorf("Error: %s", err)
	}
	file.Close()
	return nil
}

// UploadBytes to a file
func (f *IftpImpl) UploadBytes(rpath string, b []byte) error {
	var file *os.File
	var err error

	if err = f.conn.Stor(rpath, bytes.NewReader(b)); err != nil {
		return fmt.Errorf("Error: %s", err)
	}
	file.Close()
	return nil
}

// Delete a file
func (f *IftpImpl) Delete(path string) error {

	if err := f.conn.Dele(path); err != nil {
		return fmt.Errorf("failed to remove %s, %s", path, err)
	}
	return nil
}

// Exists returns true if the path exists already
func (f *IftpImpl) Exists(path string) (bool, error) {

	var err error
	var lines []string

	if lines, err = f.conn.Stat(path); err != nil {
		return false, err
	}

	//fmt.Println("stats says:", lines, " len:", len(lines))
	// NOTE: why is the array len 1 when there is nothing in it!!
	return len(lines) > 1, nil
}
